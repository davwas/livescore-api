﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebApi.Models
{
    public class PlayerInfo
    {
        public PlayerInfo()
        {
            Injuries = new List<InjuryInfo>();
        }

        [JsonProperty("common_name")]
        public string Name { get; set; }
        [JsonProperty("firstname")]
        public string FirstName { get; set; }
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        [JsonProperty("nationality")]
        public string Nationality { get; set; }
        [JsonProperty("birthdate")]
        public string BirthDate { get; set; }
        public int Age { get; set; }
        [JsonProperty("height")]
        public string Height { get; set; }
        [JsonProperty("weight")]
        public string Weight { get; set; }
        [JsonProperty("image_path")]
        public string Image { get; set; }
        [JsonProperty("team_name")]
        public string TeamName { get; set; }
        [JsonProperty("team_id")]
        public string TeamID { get; set; }
        [JsonProperty("logo_path")]
        public string TeamLogo { get; set; }
        [JsonProperty("minutes")]
        public int Minutes { get; set; }
        [JsonProperty("appearences")]
        public int Appearences { get; set; }
        [JsonProperty("goals")]
        public int Goals { get; set; }
        [JsonProperty("yellowcards")]
        public int YellowCards { get; set; }
        [JsonProperty("yellowred")]
        public int YellowReds { get; set; }
        [JsonProperty("redcards")]
        public int RedCards { get; set; }
        [JsonProperty("position_name")]
        public string Position { get; set; }
        public List<InjuryInfo> Injuries { get; set; }
    }
}
