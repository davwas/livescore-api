﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class InjuryInfo
    {
        [JsonProperty("start_date")]
        public string StartDate { get; set; }
        [JsonProperty("end_date")]
        public string EndDate { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
