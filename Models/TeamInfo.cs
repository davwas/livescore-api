﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebApi.Models
{
    public class TeamInfo
    {
        public TeamInfo()
        {
            Coach = new CoachInfo();
            Venue = new VenueInfo();
            LatestGames = new List<MatchInfo>();
            SquadPlayers = new List<SquadPlayerInfo>();
        }

        [JsonProperty("name")]
        public string TeamName { get; set; }
        [JsonProperty("logo_path")]
        public string TeamLogo { get; set; }
        [JsonProperty("founded")]
        public int Founded { get; set; }
        public CoachInfo Coach { get; set; }
        public VenueInfo Venue { get; set; }
        public List<MatchInfo> LatestGames { get; set; }
        public List<SquadPlayerInfo> SquadPlayers { get; set; }
    }
}
