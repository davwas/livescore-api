﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class AwayTeamInfo
    {
        [JsonProperty("name")]
        public string AwayTeamName { get; set; }
        [JsonProperty("short_code")]
        public string AwayTeamShortName { get; set; }
        [JsonProperty("logo_path")]
        public string AwayTeamLogo { get; set; }
        [JsonProperty("visitorteam_score")]
        public int AwayTeamScore { get; set; }
        public int AwayTeamRedCards { get; set; }
    }
}
