﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class Head2HeadInfo
    {
        public Head2HeadInfo()
        {
            HomeTeamInfo = new HomeTeamInfo();
            AwayTeamInfo = new AwayTeamInfo();
        }

        [JsonProperty("localteam_id")]
        public string HomeTeamID { get; set; }
        [JsonProperty("visitorteam_id")]
        public string AwayTeamID { get; set; }
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("ft_score")]
        public string MatchFTScore { get; set; }
        [JsonProperty("ht_score")]
        public string MatchHTScore { get; set; }
        [JsonProperty("minute")]
        public string MatchMinute { get; set; }
        public string MatchWinner { get; set; }

        public HomeTeamInfo HomeTeamInfo { get; set; }
        public AwayTeamInfo AwayTeamInfo { get; set; }
    }
}
