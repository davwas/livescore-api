﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class SquadPlayerInfo
    {
        [JsonProperty("player_id")]
        public string PlayerID { get; set; }
        [JsonProperty("appearences")]
        public int Appearences { get; set; }
        [JsonProperty("number")]
        public string Number { get; set; }
        [JsonProperty("goals")]
        public int Goals { get; set; }
        [JsonProperty("assists")]
        public int Assists { get; set; }
        [JsonProperty("yellowcards")]
        public int YellowCards { get; set; }
        [JsonProperty("redcards")]
        public int RedCards { get; set; }
        [JsonProperty("yellowred")]
        public int YellowReds { get; set; }
        [JsonProperty("common_name")]
        public string Name { get; set; }
        public int Age { get; set; }
        [JsonProperty("nationality")]
        public string Nationality { get; set; }
        [JsonProperty("name")]
        public string PositionName { get; set; }
    }
}
