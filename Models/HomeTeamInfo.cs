﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class HomeTeamInfo
    {
        [JsonProperty("name")]
        public string HomeTeamName { get; set; }
        [JsonProperty("short_code")]
        public string HomeTeamShortName { get; set; }
        [JsonProperty("logo_path")]
        public string HomeTeamLogo { get; set; }
        [JsonProperty("localteam_score")]
        public int HomeTeamScore { get; set; }
        public int HomeTeamRedCards { get; set; }
    }
}
