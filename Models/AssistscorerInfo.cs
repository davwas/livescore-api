﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class AssistscorerInfo
    {
        [JsonProperty("position")]
        public int Position { get; set; }
        [JsonProperty("assists")]
        public int Assists { get; set; }
        [JsonProperty("common_name")]
        public string Name { get; set; }
        public int Age { get; set; }
        [JsonProperty("nationality")]
        public string Nationality { get; set; }
        [JsonProperty("image_path")]
        public string ImagePath { get; set; }
        [JsonProperty("name")]
        public string TeamName { get; set; }
        [JsonProperty("logo_path")]
        public string TeamLogoPath { get; set; }
        [JsonProperty("player_id")]
        public string PlayerID { get; set; }
    }
}
