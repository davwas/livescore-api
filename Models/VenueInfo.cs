﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class VenueInfo
    {
        [JsonProperty("name")]
        public string VenueName { get; set; }
        [JsonProperty("capacity")]
        public string Capacity { get; set; }
        [JsonProperty("image_path")]
        public string Image { get; set; }
    }
}
