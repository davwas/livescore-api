﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebApi.Models
{
    public class MatchInfo
    {
        public MatchInfo()
        {
            HomeTeam = new HomeTeamInfo();
            AwayTeam = new AwayTeamInfo();
            Events = new List<EventInfo>();
        }

        public string CountryName { get; set; }
        public string LeagueName { get; set; }
        public string CountryCode { get; set; }
        public HomeTeamInfo HomeTeam { get; set; }
        public AwayTeamInfo AwayTeam { get; set; }
        public string MatchWinner { get; set; }
        [JsonProperty("eventslist")]
        public List<EventInfo> Events { get; set; }

        [JsonProperty("status")]
        public string MatchStatus { get; set; }
        public string MatchStatusPL { get; set; }
        [JsonProperty("date")]
        public string MatchDate { get; set; }
        public string MatchTime { get; set; }
        [JsonProperty("ft_score")]
        public string MatchFTScore { get; set; }
        [JsonProperty("ht_score")]
        public string MatchHTScore { get; set; }
        [JsonProperty("minute")]
        public string MatchMinute { get; set; }
        [JsonProperty("injury_time")]
        public string InjuryMinute { get; set; }
        [JsonProperty("localteam_id")]
        public string HomeTeamID { get; set; }
        [JsonProperty("visitorteam_id")]
        public string AwayTeamID { get; set; }
    }
}
