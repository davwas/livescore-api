using Newtonsoft.Json;

namespace WebApi.Models
{
    public class EventInfo
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("team_id")]
        public string TeamID { get; set; }
        public string TeamName { get; set; }
        [JsonProperty("player_name")]
        public string PlayerName { get; set; }
        [JsonProperty("player_id")]
        public string PlayerID { get; set; }
        [JsonProperty("related_player_id")]
        public string RelatedPlayerID { get; set; }
        [JsonProperty("related_player_name")]
        public string RelatedPlayerName { get; set; }
        [JsonProperty("minute")]
        public string Minute { get; set; }
        [JsonProperty("extra_minute")]
        public string ExtraMinute { get; set; }

    }
}
