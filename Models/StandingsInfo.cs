﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebApi.Models
{
    public class StandingsInfo
    {
        public StandingsInfo()
        {
            FormArray = new List<char>();
        }

        [JsonProperty("team_id")]
        public string TeamID { get; set; }
        [JsonProperty("logo_path")]
        public string TeamLogo { get; set; }
        [JsonProperty("position")]
        public int Position { get; set; }
        [JsonProperty("team_name")]
        public string TeamName { get; set; }
        [JsonProperty("games_played")]
        public int GamesPlayed { get; set; }
        [JsonProperty("won")]
        public int GamesWon { get; set; }
        [JsonProperty("draw")]
        public int GamesDrawn { get; set; }
        [JsonProperty("lost")]
        public int GamesLost { get; set; }
        [JsonProperty("goals_scored")]
        public int GoalsFor { get; set; }
        [JsonProperty("goals_against")]
        public int GoalsAgainst { get; set; }
        [JsonProperty("points")]
        public int Points { get; set; }
        [JsonProperty("goal_difference")]
        public string GoalDifference { get; set; }
        [JsonProperty("recent_form")]
        public string Form { get; set; }
        public List<char> FormArray { get; set; }
        [JsonProperty("result")]
        public string Result { get; set; }
    }
}
