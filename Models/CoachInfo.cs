﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class CoachInfo
    {
        [JsonProperty("common_name")]
        public string CoachName { get; set; }
        [JsonProperty("firstname")]
        public string FirstName { get; set; }
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        public int Age { get; set; }
        [JsonProperty("nationality")]
        public string CoachNationality { get; set; }
        [JsonProperty("image_path")]
        public string CoachImage { get; set; }
    }
}
