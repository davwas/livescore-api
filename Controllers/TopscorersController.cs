﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TopscorersController : ControllerBase
    {
        [HttpGet("[action]")]
        public List<GoalscorerInfo> FetchGoalscorersInfo(string league)
        {

            var goalscorersList = new List<GoalscorerInfo>();
            var seasonID = GetSeasonID(league);
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchGoalscorers = "topscorers/season/" + seasonID + token + timezone + "&include=goalscorers.player,goalscorers.team";
                var fetchGoalscorersResponse = client.GetAsync(fetchGoalscorers);
                fetchGoalscorersResponse.Wait();
                var fetchGoalscorersResult = fetchGoalscorersResponse.Result;

                if (fetchGoalscorersResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchGoalscorersResult.Content.ReadAsStringAsync().Result);
                    var goalscorersData = JArray.Parse(jObject["data"]["goalscorers"]["data"].ToString());
                    var i = 0;
                    foreach (var item in goalscorersData)
                    {
                        if (i == 25) break;
                        var goalscorersInfo = new GoalscorerInfo();
                        JsonConvert.PopulateObject(item.ToString(), goalscorersInfo);
                        JsonConvert.PopulateObject(item["player"]["data"].ToString(), goalscorersInfo);
                        JsonConvert.PopulateObject(item["team"]["data"].ToString(), goalscorersInfo);
                        var birthdate = item["player"]["data"]["birthdate"].ToString();
                        if (birthdate == "")
                        {
                            birthdate = "01/01/1994";
                        }
                        if (goalscorersInfo.Nationality == null)
                        {
                            goalscorersInfo.Nationality = "Unknown";
                        }
                        var birthyear = birthdate.Remove(0, birthdate.Length - 4);
                        goalscorersInfo.Age = 2019 - Convert.ToInt32(birthyear);
                        goalscorersList.Add(goalscorersInfo);
                        i++;
                    }
                    goalscorersList.Sort((x,y) => x.Position.CompareTo(y.Position));
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchGoalscorersResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return goalscorersList;
        }

        [HttpGet("[action]")]
        public List<AssistscorerInfo> FetchAssistscorersInfo(string league)
        {

            var assistscorersList = new List<AssistscorerInfo>();
            var seasonID = GetSeasonID(league);
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchAssistscorers = "topscorers/season/" + seasonID + token + timezone + "&include=assistscorers.player,assistscorers.team";
                var fetchAssistscorersResponse = client.GetAsync(fetchAssistscorers);
                fetchAssistscorersResponse.Wait();
                var fetchAssistscorersResult = fetchAssistscorersResponse.Result;

                if (fetchAssistscorersResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchAssistscorersResult.Content.ReadAsStringAsync().Result);
                    var assistscorersData = JArray.Parse(jObject["data"]["assistscorers"]["data"].ToString());
                    var i = 0;
                    foreach (var item in assistscorersData)
                    {
                        if (i == 25) break;
                        var assistscorerInfo = new AssistscorerInfo();
                        JsonConvert.PopulateObject(item.ToString(), assistscorerInfo);
                        JsonConvert.PopulateObject(item["player"]["data"].ToString(), assistscorerInfo);
                        JsonConvert.PopulateObject(item["team"]["data"].ToString(), assistscorerInfo);
                        var birthdate = item["player"]["data"]["birthdate"].ToString();
                        if (birthdate == "")
                        {
                            birthdate = "01/01/1994";
                        }
                        if (assistscorerInfo.Nationality == null)
                        {
                            assistscorerInfo.Nationality = "Netherlands";
                        }
                        var birthyear = birthdate.Remove(0, birthdate.Length - 4);
                        assistscorerInfo.Age = 2019 - Convert.ToInt32(birthyear);
                        assistscorersList.Add(assistscorerInfo);
                        i++;
                    }
                    assistscorersList.Sort((x,y) => x.Position.CompareTo(y.Position));
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchAssistscorersResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return assistscorersList;
        }

        public string GetSeasonID(string league)
        {
            var seasonID = "";

            if (league == "La Liga")
            {
                seasonID = "13133";
            }
            else if (league == "Bundesliga")
            {
                seasonID = "13005";
            }
            else if (league == "Premier League")
            {
                seasonID = "12962";
            }
            else if (league == "Ligue 1")
            {
                seasonID = "12935";
            }
            else if (league == "Serie A")
            {
                seasonID = "13158";
            }
            return seasonID;
        }
    }
}