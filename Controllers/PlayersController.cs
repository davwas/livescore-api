﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        [HttpGet("[action]")]
        public PlayerInfo FetchPlayerInfo(string player_id)
        {
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";
            var playerInfo = new PlayerInfo();
            var injuriesListAll = new List<InjuryInfo>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchPlayer = "players/" + player_id + token + timezone + "&include=position,team,sidelined:order(start_date|desc),stats,stats.season";
                var fetchPlayerResponse = client.GetAsync(fetchPlayer);
                fetchPlayerResponse.Wait();
                var fetchPlayerResult = fetchPlayerResponse.Result;

                if (fetchPlayerResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchPlayerResult.Content.ReadAsStringAsync().Result);
                    var playerData = JObject.Parse(jObject["data"].ToString());
                    JsonConvert.PopulateObject(playerData.ToString(), playerInfo);
                    if (playerData["team"] != null)
                    {
                        JsonConvert.PopulateObject(playerData["team"]["data"].ToString(), playerInfo);
                        playerInfo.TeamName = playerData["team"]["data"]["name"].ToString();
                    }
                    playerInfo.Position = playerData["position"]["data"]["name"].ToString();
                    var birthdate = playerData["birthdate"].ToString();
                    if (birthdate == "")
                    {
                        birthdate = "01/01/1994";
                    }
                    if (playerInfo.Nationality == null)
                    {
                        playerInfo.Nationality = "Unknown";
                    }
                    var birthyear = birthdate.Remove(0, birthdate.Length - 4);
                    playerInfo.Age = 2019 - Convert.ToInt32(birthyear);

                    var playerStats = JArray.Parse(playerData["stats"]["data"].ToString());
                    foreach (var item in playerStats)
                    {
                        var seasonData = item["season"]?["data"].Value<bool?>("is_current_season") ?? false;
                        if (seasonData == true)
                        {
                            JsonConvert.PopulateObject(item.ToString(), playerInfo);
                        }
                    }

                    var playerInjuries = JArray.Parse(playerData["sidelined"]["data"].ToString());
                    foreach (var item in playerInjuries)
                    {
                        var injury = new InjuryInfo();
                        JsonConvert.PopulateObject(item.ToString(), injury);
                        injury.StartDate = Convert.ToDateTime(injury.StartDate).ToString("dd.MM.yyyy");
                        injury.EndDate = Convert.ToDateTime(injury.EndDate).ToString("dd.MM.yyyy");
                        if (injury.Description == "Knock")
                        {
                            injury.Description = "Stłuczenie";
                        }
                        else if (injury.Description == "Foot Injury")
                        {
                            injury.Description = "Kontuzja stopy";
                        }
                        else if (injury.Description == "Foot Injury")
                        {
                            injury.Description = "Kontuzja stopy";
                        }
                        else if (injury.Description == "Knee Injury")
                        {
                            injury.Description = "Kontuzja kolana";
                        }
                        else if (injury.Description == "Shoulder Injury")
                        {
                            injury.Description = "Kontuzja barku";
                        }
                        else if (injury.Description == "Hamstring")
                        {
                            injury.Description = "Kontuzja mięśnia";
                        }
                        else if (injury.Description == "Ankle/Foot Injury")
                        {
                            injury.Description = "Kontuzja stopy";
                        }
                        else if (injury.Description == "Illness")
                        {
                            injury.Description = "Choroba";
                        }
                        else if (injury.Description == "Suspended")
                        {
                            injury.Description = "Zawieszenie";
                        }
                        else if (injury.Description == "Rib Fracture")
                        {
                            injury.Description = "Pęknięte żebro";
                        }
                        else if (injury.Description == "Groin Strain")
                        {
                            injury.Description = "Kontuzja pachwiny";
                        }
                        else if (injury.Description == "Broken Toe")
                        {
                            injury.Description = "Złamany palec";
                        }
                        else if (injury.Description == "Thigh Muscle Strain")
                        {
                            injury.Description = "Kontuzja mięśnia";
                        }
                        else if (injury.Description == "Broken Arm")
                        {
                            injury.Description = "Złamana ręka";
                        }
                        else if (injury.Description == "Broken Leg")
                        {
                            injury.Description = "Złamana noga";
                        }
                        else if (injury.Description == "Renal Colic")
                        {
                            injury.Description = "Kolka nerkowa";
                        }
                        else if (injury.Description == "Virus")
                        {
                            injury.Description = "Choroba";
                        }
                        else if (injury.Description == "Groin/Pelvis Injury")
                        {
                            injury.Description = "Kontuzja pachwiny";
                        }
                        else if (injury.Description == "Calf Muscle Strain")
                        {
                            injury.Description = "Kontuzja łydki";
                        }
                        else if (injury.Description == "Abdominal Strain")
                        {
                            injury.Description = "Kontuzja mięśnia brzucha";
                        }
                        else if (injury.Description == "ACL Knee Ligament Injury")
                        {
                            injury.Description = "Zerwane wiązadła";
                        }
                        else if (injury.Description == "Ankle Fracture")
                        {
                            injury.Description = "Pęknięta kostka";
                        }
                        else
                        {
                            injury.Description = "Kontuzja";
                        }
                        injuriesListAll.Add(injury);
                    }

                    injuriesListAll.Reverse();
                    if (injuriesListAll.Count > 10)
                    {
                        injuriesListAll.RemoveRange(10, injuriesListAll.Count - 10);
                    }
                    playerInfo.Injuries = injuriesListAll;
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchPlayerResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return playerInfo;
        }
    }
}