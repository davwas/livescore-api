﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApi.Models;
using System.Diagnostics;
using System.Net.Http;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        [HttpGet("[action]")]
        public TeamInfo FetchTeamInfo(string team_id)
        {
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";
            var teamInfo = new TeamInfo();
            var latestGames = new List<MatchInfo>();
            var squadPlayers = new List<SquadPlayerInfo>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchTeam = "teams/" + team_id + token + timezone + "&include=coach,squad:order(position_id|asc),squad.player,squad.position,venue,latest:limit(10|1),latest.localTeam,latest.visitorTeam";
                var fetchTeamResponse = client.GetAsync(fetchTeam);
                fetchTeamResponse.Wait();
                var fetchTeamResult = fetchTeamResponse.Result;

                if (fetchTeamResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchTeamResult.Content.ReadAsStringAsync().Result);

                    var teamData = JObject.Parse(jObject["data"].ToString());
                    JsonConvert.PopulateObject(teamData.ToString(), teamInfo);
                    JsonConvert.PopulateObject(teamData["coach"]["data"].ToString(), teamInfo.Coach);
                    var firstNames = teamInfo.Coach.FirstName.Split(' ');
                    var lastNames = teamInfo.Coach.LastName.Split(' ');
                    teamInfo.Coach.FirstName = firstNames[0];
                    teamInfo.Coach.LastName = lastNames[0];
                    JsonConvert.PopulateObject(teamData["venue"]["data"].ToString(), teamInfo.Venue);
                    var birthdate = teamData["coach"]["data"]["birthdate"].ToString();
                    var birthyear = birthdate.Remove(0, birthdate.Length - 4);
                    teamInfo.Coach.Age = 2019 - Convert.ToInt32(birthyear);
                    var squadData = JArray.Parse(jObject["data"]["squad"]["data"].ToString());
                    foreach (var item in squadData)
                    {
                        var squadPlayer = new SquadPlayerInfo();
                        JsonConvert.PopulateObject(item.ToString(), squadPlayer);
                        if (squadPlayer.Number == null) squadPlayer.Number = "1";
                        JsonConvert.PopulateObject(item["player"]["data"].ToString(), squadPlayer);
                        if (item["position"] != null)
                        {
                            JsonConvert.PopulateObject(item["position"]["data"].ToString(), squadPlayer);
                        }
                        var birthdate1 = item["player"]["data"]["birthdate"].ToString();
                        if (birthdate1 == "")
                        {
                            birthdate1 = "01/01/1994";
                        }
                        if (squadPlayer.Nationality == null)
                        {
                            squadPlayer.Nationality = "Unknown";
                        }
                        var birthyear1 = birthdate1.Remove(0, birthdate1.Length - 4);
                        squadPlayer.Age = 2019 - Convert.ToInt32(birthyear1);
                        squadPlayer.RedCards += squadPlayer.YellowReds;
                        squadPlayers.Add(squadPlayer);
                    }
                    teamInfo.SquadPlayers = squadPlayers;
                    var matchData = JArray.Parse(jObject["data"]["latest"]["data"].ToString());
                    foreach (var item in matchData)
                    {
                        var matchInfo = new MatchInfo();
                        JsonConvert.PopulateObject(item.ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), matchInfo.HomeTeam);
                        JsonConvert.PopulateObject(item["scores"].ToString(), matchInfo.AwayTeam);
                        var time = item["time"]["starting_at"]["time"].ToString();
                        matchInfo.MatchTime = time.Remove(time.Length - 3);
                        JsonConvert.PopulateObject(item["time"].ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["time"]["starting_at"].ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["localTeam"]["data"].ToString(), matchInfo.HomeTeam);
                        JsonConvert.PopulateObject(item["visitorTeam"]["data"].ToString(), matchInfo.AwayTeam);
                        matchInfo.MatchDate = Convert.ToDateTime(matchInfo.MatchDate).ToString("dd.MM.yyyy");
                        if (matchInfo.HomeTeam.HomeTeamScore > matchInfo.AwayTeam.AwayTeamScore)
                        {
                            matchInfo.MatchWinner = matchInfo.HomeTeam.HomeTeamName;
                        }
                        else if (matchInfo.HomeTeam.HomeTeamScore < matchInfo.AwayTeam.AwayTeamScore)
                        {
                            matchInfo.MatchWinner = matchInfo.AwayTeam.AwayTeamName;
                        }
                        latestGames.Add(matchInfo);
                    }
                    teamInfo.LatestGames = latestGames;
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchTeamResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return teamInfo;
        }
    }
}