﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StandingsController : ControllerBase
    {
        [HttpGet("[action]")]
        public List<StandingsInfo> FetchStandingsInfo(string league)
        {

            var standingsList = new List<StandingsInfo>();
            var seasonID = GetSeasonID(league);
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchStandings = "standings/season/" + seasonID + token + timezone + "&include=standings.team";
                var fetchStandingsResponse = client.GetAsync(fetchStandings);
                fetchStandingsResponse.Wait();
                var fetchStandingsResult = fetchStandingsResponse.Result;

                if (fetchStandingsResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchStandingsResult.Content.ReadAsStringAsync().Result);
                    var standingsData = JArray.Parse(jObject["data"][0]["standings"]["data"].ToString());

                    foreach (var item in standingsData)
                    {
                        var standingsInfo = new StandingsInfo();
                        JsonConvert.PopulateObject(item.ToString(), standingsInfo);
                        JsonConvert.PopulateObject(item["overall"].ToString(), standingsInfo);
                        JsonConvert.PopulateObject(item["total"].ToString(), standingsInfo);
                        JsonConvert.PopulateObject(item["team"]["data"].ToString(), standingsInfo);
                        foreach (var c in standingsInfo.Form)
                        {
                            standingsInfo.FormArray.Add(c);
                        }
                        //standingsInfo.FormArray.Reverse();
                        standingsList.Add(standingsInfo);
                    }
                    standingsList.Sort((x,y) => x.Position.CompareTo(y.Position));
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchStandingsResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return standingsList;
        }

        public string GetSeasonID(string league)
        {
            var seasonID = "";

            if (league == "La Liga")
            {
                seasonID = "13133";
            }
            else if (league == "Bundesliga")
            {
                seasonID = "13005";
            }
            else if (league == "Premier League")
            {
                seasonID = "12962";
            }
            else if (league == "Ligue 1")
            {
                seasonID = "12935";
            }
            else if (league == "Serie A")
            {
                seasonID = "13158";
            }
            return seasonID;
        }
    }
}