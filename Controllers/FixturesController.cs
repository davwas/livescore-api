﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FixturesController : ControllerBase
    {
        [HttpGet("[action]")]
        public List<List<MatchInfo>> FetchAllMatches(string date)
        {
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";
            var matchList = new List<MatchInfo>();
            var countryMatchList = new List<List<MatchInfo>>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchGames = "fixtures/between/" + date + "/" + date + token + timezone + "&include=localTeam,visitorTeam,league,cards,events";
                var fetchGamesResponse = client.GetAsync(fetchGames);
                fetchGamesResponse.Wait();
                var fetchGamesResult = fetchGamesResponse.Result;

                if (fetchGamesResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchGamesResult.Content.ReadAsStringAsync().Result);
                    var matchData = JArray.Parse(jObject["data"].ToString());

                    foreach (var item in matchData)
                    {
                        var matchInfo = new MatchInfo();
                        var countryAndLeague = GetLeagueAndCountryNames(item["league_id"].ToString());
                        matchInfo.CountryName = countryAndLeague[0];
                        matchInfo.LeagueName = countryAndLeague[1];
                        matchInfo.CountryCode = countryAndLeague[2];
                        JsonConvert.PopulateObject(item.ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), matchInfo.HomeTeam);
                        JsonConvert.PopulateObject(item["scores"].ToString(), matchInfo.AwayTeam);
                        var time = item["time"]["starting_at"]["time"].ToString();
                        matchInfo.MatchTime = time.Remove(time.Length - 3);
                        JsonConvert.PopulateObject(item["time"].ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["time"]["starting_at"].ToString(), matchInfo);
                        JsonConvert.PopulateObject(item["localTeam"]["data"].ToString(), matchInfo.HomeTeam);
                        JsonConvert.PopulateObject(item["visitorTeam"]["data"].ToString(), matchInfo.AwayTeam);
                        matchInfo.MatchDate = Convert.ToDateTime(matchInfo.MatchDate).ToString("dd.MM.yyyy"); ;
                        foreach (var card in item["cards"]["data"])
                        {
                            var teamID = card["team_id"].ToString();
                            var type = card["type"].ToString();
                            if ((type == "redcard" || type == "yellowred") && teamID == matchInfo.HomeTeamID)
                            {
                                matchInfo.HomeTeam.HomeTeamRedCards += 1;
                            }
                            else if ((type == "redcard" || type == "yellowred") && teamID == matchInfo.AwayTeamID)
                            {
                                matchInfo.AwayTeam.AwayTeamRedCards += 1;
                            }
                        }

                        foreach (var ev in item["events"]["data"])
                        {
                            var _event = new EventInfo();
                            JsonConvert.PopulateObject(ev.ToString(), _event);
                            if (_event.TeamID == matchInfo.HomeTeamID)
                            {
                                _event.TeamName = matchInfo.HomeTeam.HomeTeamName;
                            }
                            else
                            {
                                _event.TeamName = matchInfo.AwayTeam.AwayTeamName;
                            }
                            if (_event.ExtraMinute != null)
                            {
                                _event.Minute = (Convert.ToInt32(_event.Minute) + Convert.ToInt32(_event.ExtraMinute)).ToString();
                            }
                            matchInfo.Events.Add(_event);
                        }

                        if (matchInfo.MatchHTScore != null)
                        {
                            matchInfo.MatchHTScore = "(" + matchInfo.MatchHTScore[0] + " - " + matchInfo.MatchHTScore[2] + ")";
                        }
                        if (matchInfo.InjuryMinute != null)
                        {
                            matchInfo.MatchMinute = (Int32.Parse(matchInfo.MatchMinute) + Int32.Parse(matchInfo.InjuryMinute)).ToString();
                        }
                        if (matchInfo.HomeTeam.HomeTeamScore > matchInfo.AwayTeam.AwayTeamScore)
                        {
                            matchInfo.MatchWinner = matchInfo.HomeTeam.HomeTeamName;
                        }
                        else if (matchInfo.HomeTeam.HomeTeamScore < matchInfo.AwayTeam.AwayTeamScore)
                        {
                            matchInfo.MatchWinner = matchInfo.AwayTeam.AwayTeamName;
                        }
                        matchInfo.MatchStatusPL = GetMatchStatusPL(matchInfo.MatchStatus);
                        matchList.Add(matchInfo);
                    }
                    var englandList = new List<MatchInfo>();
                    var germanyList = new List<MatchInfo>();
                    var franceList = new List<MatchInfo>();
                    var spainList = new List<MatchInfo>();
                    var italyList = new List<MatchInfo>();
                    foreach (var item in matchList)
                    {
                        if (item.CountryName == "Anglia")
                        {
                            englandList.Add(item);
                        }
                        if (item.CountryName == "Niemcy")
                        {
                            germanyList.Add(item);
                        }
                        if (item.CountryName == "Francja")
                        {
                            franceList.Add(item);
                        }
                        if (item.CountryName == "Hiszpania")
                        {
                            spainList.Add(item);
                        }
                        if (item.CountryName == "Włochy")
                        {
                            italyList.Add(item);
                        }
                    }
                    countryMatchList.Add(englandList);
                    countryMatchList.Add(germanyList);
                    countryMatchList.Add(franceList);
                    countryMatchList.Add(spainList);
                    countryMatchList.Add(italyList);
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchGamesResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return countryMatchList;
        }

        [HttpGet("[action]")]
        public List<List<MatchInfo>> FetchLiveMatches(string date)
        {
            var allMatches = FetchAllMatches(date);
            var countryMatchLiveList = new List<List<MatchInfo>>();

            foreach (var item in allMatches)
            {
                var countryList = new List<MatchInfo>();
                foreach (var item2 in item)
                {
                    if (item2.MatchStatus == "LIVE" || item2.MatchStatus == "HT")
                    {
                        countryList.Add(item2);
                    }
                }
                countryMatchLiveList.Add(countryList);
            }

            return countryMatchLiveList;
        }

        [HttpGet("[action]")]
        public List<List<MatchInfo>> FetchEndedMatches(string date)
        {
            var allMatches = FetchAllMatches(date);
            var countryMatchEndedList = new List<List<MatchInfo>>();

            foreach (var item in allMatches)
            {
                var countryList = new List<MatchInfo>();
                foreach (var item2 in item)
                {
                    if (item2.MatchStatus == "FT")
                    {
                        countryList.Add(item2);
                    }
                }
                countryMatchEndedList.Add(countryList);
            }

            return countryMatchEndedList;
        }

        [HttpGet("[action]")]
        public List<List<MatchInfo>> FetchScheduledMatches(string date)
        {
            var allMatches = FetchAllMatches(date);
            var countryMatchScheduledList = new List<List<MatchInfo>>();

            foreach (var item in allMatches)
            {
                var countryList = new List<MatchInfo>();
                foreach (var item2 in item)
                {
                    if (item2.MatchStatus == "NS")
                    {
                        countryList.Add(item2);
                    }
                }
                countryMatchScheduledList.Add(countryList);
            }

            return countryMatchScheduledList;
        }

        public string[] GetLeagueAndCountryNames(string league_id)
        {
            string[] leagueAndCountry = new string[3];
            if (league_id == "301")
            {
                string countryName = "Francja";
                string leagueName = "Ligue 1";
                string countryCode = "fr";
                leagueAndCountry[0] = countryName;
                leagueAndCountry[1] = leagueName;
                leagueAndCountry[2] = countryCode;
            }
            else if (league_id == "384")
            {
                string countryName = "Włochy";
                string leagueName = "Serie A";
                string countryCode = "it";
                leagueAndCountry[0] = countryName;
                leagueAndCountry[1] = leagueName;
                leagueAndCountry[2] = countryCode;
            }
            else if (league_id == "8")
            {
                string countryName = "Anglia";
                string leagueName = "Premier League";
                string countryCode = "gb-eng";
                leagueAndCountry[0] = countryName;
                leagueAndCountry[1] = leagueName;
                leagueAndCountry[2] = countryCode;
            }
            else if (league_id == "564")
            {
                string countryName = "Hiszpania";
                string leagueName = "La Liga";
                string countryCode = "es";
                leagueAndCountry[0] = countryName;
                leagueAndCountry[1] = leagueName;
                leagueAndCountry[2] = countryCode;
            }
            else if (league_id == "82")
            {
                string countryName = "Niemcy";
                string leagueName = "Bundesliga";
                string countryCode = "de";
                leagueAndCountry[0] = countryName;
                leagueAndCountry[1] = leagueName;
                leagueAndCountry[2] = countryCode;
            }
            return leagueAndCountry;
        }

        public string GetMatchStatusPL(string matchStatus)
        {
            var matchStatusPL = "";
            switch (matchStatus)
            {
                case "NS":
                    matchStatusPL = "";
                    break;
                case "LIVE":
                    matchStatusPL = "LIVE";
                    break;
                case "HT":
                    matchStatusPL = "Przerwa";
                    break;
                case "FT":
                    matchStatusPL = "Koniec";
                    break;
                case "CANCL":
                    matchStatusPL = "Anulowany";
                    break;
                case "POSTP":
                    matchStatusPL = "Przełożony";
                    break;
                case "Delayed":
                    matchStatusPL = "Opóźniony";
                    break;
                default:
                    break;
            }

            return matchStatusPL;
        }
    }
}