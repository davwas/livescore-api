﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Head2HeadController : ControllerBase
    {
        [HttpGet("[action]")]
        public List<Head2HeadInfo> FetchHead2HeadInfo(string homeTeamID, string awayTeamID)
        {
            var token = "?api_token=u0jgdukCXubYKma9qWKVaJvauf6dtRgDxjxTtlt59SFLlINNSZhVMqL3keYx";
            var timezone = "&tz=Europe/Warsaw";
            var h2hList = new List<Head2HeadInfo>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://soccer.sportmonks.com/api/v2.0/");

                var fetchH2h = "head2head/" + homeTeamID + "/" + awayTeamID + token + timezone + "&include=localTeam,visitorTeam,cards";
                var fetchH2hResponse = client.GetAsync(fetchH2h);
                fetchH2hResponse.Wait();
                var fetchH2hResult = fetchH2hResponse.Result;

                if (fetchH2hResult.IsSuccessStatusCode)
                {
                    var jObject = JObject.Parse(fetchH2hResult.Content.ReadAsStringAsync().Result);
                    var h2hData = JArray.Parse(jObject["data"].ToString());

                    foreach (var item in h2hData)
                    {
                        var h2hInfo = new Head2HeadInfo();
                        JsonConvert.PopulateObject(item.ToString(), h2hInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), h2hInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), h2hInfo.HomeTeamInfo);
                        JsonConvert.PopulateObject(item["scores"].ToString(), h2hInfo.AwayTeamInfo);
                        JsonConvert.PopulateObject(item["time"].ToString(), h2hInfo);
                        JsonConvert.PopulateObject(item["time"]["starting_at"].ToString(), h2hInfo);
                        JsonConvert.PopulateObject(item["localTeam"]["data"].ToString(), h2hInfo.HomeTeamInfo);
                        JsonConvert.PopulateObject(item["visitorTeam"]["data"].ToString(), h2hInfo.AwayTeamInfo);
                        h2hInfo.Date = Convert.ToDateTime(h2hInfo.Date).ToString("dd.MM.yyyy");
                        foreach (var card in item["cards"]["data"])
                        {
                            var teamID = card["team_id"].ToString();
                            var type = card["type"].ToString();
                            if ((type == "redcard" || type == "yellowred") && teamID == h2hInfo.HomeTeamID)
                            {
                                h2hInfo.HomeTeamInfo.HomeTeamRedCards += 1;
                            }
                            else if ((type == "redcard" || type == "yellowred") && teamID == h2hInfo.AwayTeamID)
                            {
                                h2hInfo.AwayTeamInfo.AwayTeamRedCards += 1;
                            }
                        }
                        if (h2hInfo.HomeTeamInfo.HomeTeamScore > h2hInfo.AwayTeamInfo.AwayTeamScore)
                        {
                            h2hInfo.MatchWinner = h2hInfo.HomeTeamInfo.HomeTeamName;
                        }
                        else if (h2hInfo.HomeTeamInfo.HomeTeamScore < h2hInfo.AwayTeamInfo.AwayTeamScore)
                        {
                            h2hInfo.MatchWinner = h2hInfo.AwayTeamInfo.AwayTeamName;
                        }
                        h2hList.Add(h2hInfo);
                    }
                    if (h2hList.Count > 10)
                    {
                        h2hList.RemoveRange(10, h2hList.Count - 10);
                    }
                }
                else
                {
                    Debug.WriteLine("################");
                    Debug.WriteLine(fetchH2hResult.StatusCode);
                    Debug.WriteLine("################");
                }
            }
            return h2hList;
        }
    }
}